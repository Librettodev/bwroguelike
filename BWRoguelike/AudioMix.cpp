#include "main.hpp"
#include "SDL.h"
#include <stdio.h>

static const char *MAIN_TRACK = "music.mp3";

void MyAudioMix::SetupAudio(){

	if (SDL_Init(SDL_INIT_AUDIO) < 0) {
		printf("Failed to init SDL\n");
		exit(1);
	}

	/*if (flags != (result = Mix_Init(flags))) {
		printf("Could not init mixer");
		printf("Mix_Init: %s\n", Mix_GetError());
		exit(1);
	}*/

	Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 2048);
	theMusic = Mix_LoadMUS(MAIN_TRACK);
}

void MyAudioMix::PlayAudio()
{
	Mix_Volume(-1, 20);
	Mix_PlayMusic(theMusic, -1);
	Mix_VolumeMusic(40);
}

MyAudioMix::~MyAudioMix() {
	Mix_FreeMusic(theMusic);
}


