#pragma once
class Attacker : public Persistent {
public:
	float power; //Damage dealt

	Attacker(float power);
	void attack(Actor *owner, Actor *target);
	void load(TCODZip &zip);
	void save(TCODZip &zip);
};