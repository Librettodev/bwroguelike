#pragma once
class Ai : public Persistent {
public :
	virtual void update(Actor *owner) = 0;
	virtual ~Ai() {};
	static Ai *create(TCODZip &zip);
protected:
	enum AiType {
		MONSTER,PLAYER
	};
};

class PlayerAi : public Ai {
public:
	int xpLevel;
	PlayerAi();
	int getNextLevelXp();
	void update(Actor *owner);
	void handleActionKey(Actor *owner, int ascii);
	void load(TCODZip &zip);
	void save(TCODZip &zip);
protected:
	bool moveOrAttack(Actor *owner, int targetx, int targety);
	Actor *choseFromInventory(Actor *owner);
};

class MonsterAi : public Ai {
public:
	void update(Actor *owner);
	void load(TCODZip &zip);
	void save(TCODZip &zip);
protected:
	void moveOrAttack(Actor *owner, int targetx, int targety);
	int moveCount;
};