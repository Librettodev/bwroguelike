#pragma once

static const int MAX_NAME_LENGTH = 16;

class Actor : public Persistent {
public:

	//const char *name; //actor's name
	int x, y; //position on map
	int ch; //ascii code
	TCODColor col; //color
	char name[MAX_NAME_LENGTH];
	bool blocks; //can we walk on this
	bool fovOnly; //only display when in fov
	Attacker *attacker; //can deal damage
	Destructible *destructible; // something that can be damaged
	Ai *ai; //something with a self-updating cycle
	Pickable *pickable; // something that can be picked and used;
	Container *container; // something that can contain actors;

	Actor(int x, int y, int ch, const char* name, const TCODColor &col);
	~Actor();
	float getDistance(int cx, int cy) const;
	void update();
	void render() const;

	void load(TCODZip &zip);
	void save(TCODZip &zip);
};