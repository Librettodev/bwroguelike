#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <regex>
#include <sstream>
#define __vector
class Dungeonpak;
class Reader {
	//std::ifstream fileStr;
public:
	std::vector<std::string> fileStr;
	void ReadDungeons();
	int packCount;
	std::vector<Dungeonpak> dPaks;
	void PackDungeons(std::vector<Dungeonpak> &dpak);
	void init(std::vector<Dungeonpak> &dPaks);
};

class Dungeonpak {
public:
	std::string name;
	int w;
	int h;
	//2d arrays in 1d are represented with x + y * width
	std::vector<std::string> content;
};