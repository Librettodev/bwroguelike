#pragma once

class Engine {
public:

	enum GameStatus {
		STARTUP,
		IDLE,
		NEW_TURN,
		VICTORY,
		DEFEAT
	} gameStatus;

	TCOD_mouse_t mouse;
	TCODList<Actor *> actors;
	Actor *player;
	Actor *stairs;
	Map *map;
	MyAudioMix *audMix;

	bool gameEnd;

	int screenWidth;
	int screenHeight;
	Gui *gui;
	TCOD_key_t lastKey;
	int level;
	bool stepThrough;

	Engine(int screenWidth, int screenHeight);
	~Engine();
	void nextLevel();
	void update();
	void render();
	int fovRadius;
	void sendToBack(Actor *actor);
	Actor *getClosestMonster(int x, int y, float range) const;
	bool pickATile(int *x, int *y, float maxRange = 0.0f);
	void init();
	void term();
	void load();
	void save();

private:
	bool computeFov;
};

extern Engine engine;