#include "main.hpp"
#include "SDL.h"

static const int ROOM_MAX_SIZE = 12;
static const int ROOM_MIN_SIZE = 6;
static const int MAX_ROOM_MONSTERS = 3;
static const int MAX_ROOM_ITEMS = 2;

struct BSPlet {
	TCODBsp *nodeCluster = NULL;
	bool connected;
	BSPlet() : connected(false) {}
};

class BspListener : public ITCODBspCallback {
private:
	Map &map; // a map to dig
	int roomNum; //room Number
	int lastx, lasty; //center of last room
	int leafNum = 0;
	int nodeNum = 0;
	TCODBsp *lastNode = NULL;
	TCODBsp *lastBranch = NULL;
	std::vector<BSPlet> chunks;
public:
	BspListener(Map &map) : map(map), roomNum(0) {}

	bool visitNode(TCODBsp *node, void *userData) {
		printf("\n Node Number: %d", nodeNum);
		nodeNum += 1;
		if (node->isLeaf()) {		
			printf("\n Leaf number: %d \n Node Level: %d", leafNum, node->level);
			leafNum += 1;
			int x, y, w, h;
			bool withActors = (bool)userData;
			// dig a room
			w = map.rng->getInt(ROOM_MIN_SIZE, node->w - 2);
			h = map.rng->getInt(ROOM_MIN_SIZE, node->h - 2);
			x = map.rng->getInt(node->x + 1, node->x + node->w - w - 1);
			y = map.rng->getInt(node->y + 1, node->y + node->h - h - 1);
			
			map.createRoom(roomNum == 0, x, y, x + w - 1, y + h - 1, withActors);
			if (roomNum != 0) {
				//pairs up "room cherries"
				if (node->getFather()->getLeft() == lastNode || 
					node->getFather()->getRight() == lastNode)
				{
					map.dig(lastx, lasty, x + w / 2, lasty);
					map.dig(x + w / 2, lasty, x + w / 2, y + h / 2);
					BSPlet temp;
					temp.nodeCluster = node->getFather();
					chunks.push_back(temp);
				}
				//map.dig(lastx, lasty, node->getFather()->x + node->getFather()->w / 2, lasty);
				//map.dig(node->getFather()->x + node->getFather()->w / 2, lasty, node->getFather()->x + node->getFather()->w / 2, node->getFather()->y + node->getFather()->h / 2);

			}
			lastx = x + w / 2;
			lasty = y + h / 2;
			roomNum++;
			lastNode = node;
			if (engine.stepThrough) {
				
				engine.render();
				SDL_Delay(800);
				//draws red boxes around partitions in BSP
				for (int i = node->x; i < node->x + node->w; i++) {
					for (int j = node->y; j < node->y + node->h; j++) {
						if (i != node->x && j != node->y)
						{
							if (i != node->x + node->w -1&& j != node->y + node->h -1)
								continue;
						}
					//map.tiles[i + j * map.width].bspMarked = true;
					}
				}
				TCODConsole::flush();
			}
			if (chunks.size() > 1) {
				for (int i = 0; i <= chunks.size() - 1; i++)
				{
					if (chunks.at(i).connected == false)
					{
						printf("\n Chunk %d not connected!", i);
						for (int j = 0; j < 20; j++) {
							int tempX = map.rng->getInt(chunks.at(i).nodeCluster->x, chunks.at(i).nodeCluster->x + chunks.at(i).nodeCluster->w - 1);
							int tempY = map.rng->getInt(chunks.at(i).nodeCluster->y, chunks.at(i).nodeCluster->y + chunks.at(i).nodeCluster->h - 1);
							if (map.canWalk(tempX, tempY))
							{
								printf("Found empty space!");
							}
						}
					}
				}
			}
		}
		return true;
		
	}
};

Map::Map(int width, int height) : width(width), height(height) {
	seed = TCODRandom::getInstance()->getInt(0, 0x7FFFFFFF);
}

Map::~Map() {
	delete[] tiles;
	delete map;
}

bool Map::isWall(int x, int y) const {
	return !map->isWalkable(x,y);
}

bool Map::canWalk(int x, int y) const {
	if (isWall(x, y)) {
		//this is a wall
		return false;
	}
	for (Actor **iterator = engine.actors.begin();
		iterator != engine.actors.end(); iterator++) {
		Actor *actor = *iterator;
		if (actor->x == x && actor->y == y && actor->blocks) {
			//there is a blocking actor there, can't walk
			return false;
		}
	}
	return true;
}

bool Map::isExplored(int x, int y) const {
	return tiles[x + y*width].explored;
}

bool Map::isInFov(int x, int y) const {
	if (engine.stepThrough)
		return true;
	if (x < 0 || x >= width || y < 0 || y >= height) {
		return false;
	}
	if (map->isInFov(x, y)) {
		tiles[x + y*width].explored = true;
		return true;
	}
	return false;
}

bool Map::bspCheck(int x, int y) const
{
	//checks if on bsp boundary
	return tiles[x+y*width].bspMarked;
}

void Map::computeFov() {
	if (engine.stepThrough)
		return;
	map->computeFov(engine.player->x, engine.player->y, engine.fovRadius);
}

/*void Map::setWall(int x, int y) {
	tiles[x + y*width].canWalk = false;
}*/

void Map::render() const {
	static const TCODColor darkWall(0, 0, 100);
	static const TCODColor darkGround(50, 50, 150);
	static const TCODColor lightWall(130, 110, 50);
	static const TCODColor lightGround(200, 180, 50);
	static const TCODColor bspMark = TCODColor::red;
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++)
		{
			
			if (isInFov(x, y)) {
				TCODConsole::root->setCharBackground(x, y,
					isWall(x, y) ? lightWall : lightGround);
			}
			else if (isExplored(x, y)) {
				TCODConsole::root->setCharBackground(x, y,
					isWall(x, y) ? darkWall : darkGround);
			}
			if (bspCheck(x, y)) {
				TCODConsole::root->setCharBackground(x, y, bspMark, TCOD_BKGND_SET);
			}
		}
	}
}

void Map::dig(int x1, int y1, int x2, int y2) {
	//if 2nd coord is smaller, swap
	if (x2 < x1) {
		int tmp = x2;
		x2 = x1;
		x1 = tmp;
	}
	if (y2 < y1) {
		int tmp = y2;
		y2 = y1;
		y1 = tmp;
	}
	for (int tilex = x1; tilex <= x2; tilex++) {
		for (int tiley = y1; tiley <= y2; tiley++) {
			map->setProperties(tilex, tiley, true, true);
		}
	}
}

void Map::createRoom(bool first, int x1, int y1, int x2, int y2, bool withActors) {

	if (rng->getInt(0, 10) > 11)
	{
		if (dPaks.at(0).w <= x2 - x1 && dPaks.at(0).h <= y2 - y1) {
			for (int tilex = x1; tilex <= x2; tilex++) {
				for (int tiley = y1; tiley <= y2; tiley++) {
					if (y2 - tiley > dPaks.at(0).content.size() - 1)
						continue;
					if (x2 - tilex > dPaks.at(0).content.at(y2-tiley).length())
						continue;
					if(dPaks.at(0).content.at(y2-tiley).c_str()[x2-tilex] == '.')
						map->setProperties(tilex, tiley, true, true);
				}
			}
		}
	}
	else {
	dig(x1, y1, x2, y2);
	}
	if (!withActors) {
		return;
	}
	if (first) {
		// put the player in the first room
		engine.player->x = (x1 + x2) / 2;
		engine.player->y = (y1 + y2) / 2;
	}
	else {
		//add monsters
		TCODRandom *rng = TCODRandom::getInstance();
		int nbMonsters = rng->getInt(0, MAX_ROOM_MONSTERS);
		while (nbMonsters > 0) {
			int x = rng->getInt(x1, x2);
			int y = rng->getInt(y1, y2);
			if (canWalk(x, y)) {
				addMonster(x, y);
			}
			nbMonsters--;
		}
		//add items
		int nbItems = rng->getInt(0, MAX_ROOM_ITEMS);
		while (nbItems > 0) {
			int x = rng->getInt(x1, x2);
			int y = rng->getInt(y1, y2);
			if (canWalk(x, y)) {
				addItem(x, y);
			}
			nbItems--;
		}
	}
	engine.stairs->x = (x1 + x2) / 2;
	engine.stairs->y = (y1 + y2) / 2;
}

void Map::addMonster(int x, int y) {
	TCODRandom *rng = TCODRandom::getInstance();
	if (rng->getInt(0, 100) < 80) {
		// create an orc
		Actor *orc = new Actor(x, y, 'o', "orc",
			TCODColor::desaturatedGreen);
		orc->destructible = new MonsterDestructible(10, 0, "dead orc", 50);
		orc->attacker = new Attacker(3);
		orc->ai = new MonsterAi();
		engine.actors.push(orc);
	}
	else {
		// create a troll
		Actor *troll = new Actor(x, y, 'T', "troll",
			TCODColor::darkerCyan);
		troll->destructible = new MonsterDestructible(16, 1, "troll carcass", 100);
		troll->attacker = new Attacker(4);
		troll->ai = new MonsterAi();
		engine.actors.push(troll);
	}
}

void Map::addItem(int x, int y)
{
	TCODRandom *rng = TCODRandom::getInstance();
	int dice = rng->getInt(0, 100);
	if (dice < 10) {
		// create a health potion
		Actor *healthPotion = new Actor(x, y, '!', "health potion",
			TCODColor::violet);
		healthPotion->blocks = false;
		healthPotion->pickable = new Healer(4);
		engine.actors.push(healthPotion);
	}
	else if (dice < 10 + 10) {
		// create a scroll of lightning bolt 
		Actor *scrollOfLightningBolt = new Actor(x, y, '#', "lightningbolt",
			TCODColor::lightRed);
		scrollOfLightningBolt->blocks = false;
		scrollOfLightningBolt->pickable = new LightningBolt(5, 20);
		engine.actors.push(scrollOfLightningBolt);
	}
	else if (dice < 70 + 10 + 10) {
		// create a scroll of fireball
		Actor *scrollOfFireball = new Actor(x, y, '#', "fireball",
			TCODColor::lightYellow);
		scrollOfFireball->blocks = false;
		scrollOfFireball->pickable = new Fireball(3, 12);
		engine.actors.push(scrollOfFireball);
	}
}

void Map::init(bool withActors)
{
	read.init(dPaks);
	//check if we're generating
	if (!engine.stepThrough) {
	rng = new TCODRandom(seed, TCOD_RNG_CMWC);
	tiles = new Tile[width*height];
	map = new TCODMap(width, height);
	TCODBsp bsp(0, 0, width, height);
	bsp.splitRecursive(rng, 4, ROOM_MAX_SIZE, ROOM_MAX_SIZE, 1.5f, 1.5f);
	BspListener listener(*this);
	bsp.traverseInvertedLevelOrder(&listener, (void *)withActors);
	}
	else {
		rng = new TCODRandom(seed, TCOD_RNG_CMWC);
		tiles = new Tile[width*height];
		map = new TCODMap(width, height);
		TCODBsp bsp(0, 0, width, height);
		bsp.splitRecursive(rng, 4, ROOM_MAX_SIZE, ROOM_MAX_SIZE, 1.5f, 1.5f);
		BspListener listener(*this);
		bsp.traverseInvertedLevelOrder(&listener, (void *)withActors);
	}
}

void Map::load(TCODZip & zip)
{
	seed = zip.getInt();
	init(false);
	for (int i = 0; i < width*height; i++) {
		tiles[i].explored = zip.getInt();
	}
}

void Map::save(TCODZip & zip)
{
	zip.putInt(seed);
	for (int i = 0; i < width*height; i++) {
		zip.putInt(tiles[i].explored);
	}
}
