#pragma once

class MyAudioMix
{
public:
	//simple setup and play functions
	//only plays music
	Mix_Music *theMusic;
	int result = 0;
	int flags = MIX_INIT_MP3;
	void SetupAudio();
	void PlayAudio();
	
	~MyAudioMix();
};