#pragma once
#include <vector>
#include "Reader.hpp"
#ifndef GENERATOR
struct Tile {
	bool explored;
	bool bspMarked;
	Tile() : explored(false), bspMarked(false) {}
};
#else
struct Tile {
	bool explored;
	bool bspMarked;
	Tile() : explored(true), bspMarked(false) {}
};
#endif

class Map : public Persistent{
public :
	int width, height;
	std::vector<Dungeonpak> dPaks;
	Reader read;

	Map(int width, int height);
	~Map();
	bool isWall(int x, int y) const;
	bool canWalk(int x, int y) const;
	bool isInFov(int x, int y) const;
	bool bspCheck(int x, int y) const;
	bool isExplored(int x, int y) const;
	void computeFov();
	void render() const;
	void addMonster(int x, int y);
	void addItem(int x, int y);
	void init(bool withActors);
	void load(TCODZip &zip);
	void save(TCODZip &zip);
protected:
	Tile *tiles;
	TCODMap *map;
	long seed;
	TCODRandom *rng;
	friend class BspListener;

	void dig(int x1, int y1, int x2, int y2);
	void createRoom(bool first, int x1, int y1, int x2, int y2, bool withActors);

	/*void setWall(int x, int y);*/
};