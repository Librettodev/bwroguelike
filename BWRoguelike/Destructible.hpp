#pragma once
class Destructible : public Persistent {
public:
	float maxHp; //Health points
	float hp; //current health points
	float defense; //hit point deflection
	const char *corpseName; //actor's name once dead
	int xp; //XP gained from killing this monster (or player xp)

	Destructible(float maxHp, float defense, const char *corpseName, int xp);
	virtual ~Destructible();
	inline bool isDead() { return hp <= 0; }
	float takeDamage(Actor *owner, float damage);
	float heal(float amount);
	virtual void die(Actor *owner);
	void load(TCODZip &zip);
	void save(TCODZip &zip);
	static Destructible *create(TCODZip &zip);
protected:
	enum DestructibleType {
		MONSTER,PLAYER
	};
};

class MonsterDestructible : public Destructible {
public:
	MonsterDestructible(float maxHp, float defense, const char *corpseName, int xp);
	void die(Actor *owner);
	void save(TCODZip & zip);
};

class PlayerDestructible : public Destructible {
public:
	PlayerDestructible(float maxHp, float defense, const char *corpseName, int xp);
	void die(Actor *owner);
	void save(TCODZip &zip);
};

