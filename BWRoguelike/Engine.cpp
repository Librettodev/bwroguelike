
#include <stdio.h>
#include "main.hpp"

Engine::Engine(int screenWidth, int screenHeight) : gameStatus(STARTUP), fovRadius(10), screenWidth(screenWidth), screenHeight(screenHeight), gameEnd(false), level(1) {
	TCODConsole::initRoot(screenWidth, screenHeight, "BWRoguelike v0.04", false);
	gui = new Gui();
}

Engine::~Engine() {
	term();
	delete gui;
}

void Engine::nextLevel()
{
	level++;
	gui->message(TCODColor::lightViolet, "You take a moment to rest, and recover your strength.");
	player->destructible->heal(player->destructible->maxHp / 2);
	gui->message(TCODColor::red, "After a rare moment of peace, you descend\ndeeper into the heart of the dungeon...");
	delete map;
	//delete all actors but player and stairs
	for (Actor **it = actors.begin(); it != actors.end(); it++) {
		if (*it != player && *it != stairs) {
			delete *it;
			it = actors.remove(it);
		}
	}
	// create a new map
	map = new Map(80, 43);
	map->init(true);
	gameStatus = STARTUP;

}

void Engine::update() {
	if (gameStatus == STARTUP) map->computeFov();
	gameStatus = IDLE;
	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
	if (lastKey.vk == TCODK_ESCAPE) {
		save();
		load();
	}
	player->update();
	if (gameStatus == NEW_TURN) {
		for (Actor **iterator = actors.begin(); iterator != actors.end();
			iterator++) {
			Actor *actor = *iterator;
			if (actor != player) {
				actor->update();
			}
		}
	}
}

void Engine::render() {
	TCODConsole::root->clear();
	// draw the map
	map->render();
	// draw the actors
	for (Actor **iterator = actors.begin();
		iterator != actors.end(); iterator++) {
		Actor *actor = *iterator;
		if (actor != player
			&& ((!actor->fovOnly && map->isExplored(actor->x, actor->y))
				|| map->isInFov(actor->x, actor->y))) {
			actor->render();
		}
		if (actor == player)
		{
			player->render();
		}
		//(*iterator)->render();
	}
	TCODConsole::root->print(1, screenHeight - 2, "HP : %d/%d", (int)player->destructible->hp, (int)player->destructible->maxHp);
	gui->render();
}

void Engine::sendToBack(Actor *actor)
{
	actors.remove(actor);
	actors.insertBefore(actor, 0);
}

Actor *Engine::getClosestMonster(int x, int y, float range) const
{
	Actor *closest = NULL;
	float bestDistance = 1E6f;
	for (Actor **iterator = actors.begin();
		iterator != actors.end(); iterator++) {
		Actor *actor = *iterator;
		if (actor != player && actor->destructible
			&& !actor->destructible->isDead()) {
			//check if in range and if closer than normal
			float distance = actor->getDistance(x, y);
			if (distance < bestDistance && (distance <= range || range == 0.0f)) {
				bestDistance = distance;
				closest = actor;
			}
		}
	}
	return closest;
}

bool Engine::pickATile(int * x, int * y, float maxRange)
{
	while (!TCODConsole::isWindowClosed()) {
		render();
		//highlight selectable region
		for (int cx = 0; cx < map->width; cx++) {
			for (int cy = 0; cy < map->height; cy++) {
				if (map->isInFov(cx, cy) && (maxRange == 0 || player->getDistance(cx, cy) <= maxRange)) {
					TCODColor col = TCODConsole::root->getCharBackground(cx, cy);
					col = col * 1.2f;
					TCODConsole::root->setCharBackground(cx, cy, col);
				}
			}
		}
		TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
		if (map->isInFov(mouse.cx, mouse.cy)
			&& (maxRange == 0 || player->getDistance(mouse.cx, mouse.cy) <= maxRange)) {
			TCODConsole::root->setCharBackground(mouse.cx, mouse.cy, TCODColor::white);
			if (mouse.lbutton_pressed) {
				*x = mouse.cx;
				*y = mouse.cy;
				return true;
			}
		}
		if (mouse.rbutton_pressed || (lastKey.vk != TCODK_NONE && lastKey.vk != TCODK_TEXT) ){
			return false;
		}
		TCODConsole::flush();
	}
	return false;
}

void Engine::init()
{
	player = new Actor(40, 25, '@', "player", TCODColor::white);
	player->destructible = new PlayerDestructible(30, 2, "your cadaver", 0);
	player->attacker = new Attacker(5);
	player->ai = new PlayerAi();
	player->container = new Container(26);
	actors.push(player);
	stairs = new Actor(0, 0, '>', "stairs", TCODColor::white);
	stairs->blocks = false;
	stairs->fovOnly = false;
	actors.push(stairs);
	map = new Map(80, 43);
	map->init(true);
	audMix = new MyAudioMix();
	audMix->SetupAudio();
	//audMix->PlayAudio();


gui->message(TCODColor::red,
		"Welcome stranger!\nPrepare to perish in the Tombs of the Ancient Kings.");
	gameStatus = STARTUP;
}

void Engine::term()
{
	actors.clearAndDelete();
	if (map) delete map;
	gui->clear();
}

void Engine::load()
{
	/*if (TCODSystem::fileExists("game.sav")) {
		TCODZip zip;
		zip.loadFromFile("game.sav");
		//load the map
		int width = zip.getInt();
		int height = zip.getInt();
		map = new Map(width, height);
		map->load(zip);
		//then the player
		player = new Actor(0, 0, 0, NULL, TCODColor::white);
		player->load(zip);
		actors.push(player);
		//now all the other actors
		int nbActors = zip.getInt();
		while (nbActors > 0) {
			Actor *actor = new Actor(0, 0, 0, NULL, TCODColor::white);
			actor->load(zip);
			actors.push(actor);
			nbActors--;
		}
		//finally the message log
		gui->load(zip);
	}
	else {
		engine.init();
	}*/
	engine.gui->menu.clear();
	engine.gui->menu.addItem(Menu::NEW_GAME, "New game");
	if (TCODSystem::fileExists("game.sav")) {
		engine.gui->menu.addItem(Menu::CONTINUE, "Continue");
	}
	engine.gui->menu.addItem(Menu::EXIT, "Exit");
	engine.gui->menu.addItem(Menu::GENERATE, "Generator");
	Menu::MenuItemCode menuItem = engine.gui->menu.pick();
	if (menuItem == Menu::EXIT || menuItem == Menu::NONE) {
		//exit or window closed
		exit(0);
	}
	else if (menuItem == Menu::NEW_GAME) {
		//new game
		engine.term();
		engine.init();
	}
	else if (menuItem == Menu::CONTINUE) {
		TCODZip zip;
		//continue saved
		engine.term();
		zip.loadFromFile("game.sav");
		//load the map
		int width = zip.getInt();
		int height = zip.getInt();
		map = new Map(width, height);
		map->load(zip);
		//then the player
		player = new Actor(0, 0, 0, NULL, TCODColor::white);
		player->load(zip);
		actors.push(player);
		stairs = new Actor(0, 0, 0, NULL, TCODColor::white);
		stairs->load(zip);
		actors.push(stairs);
		//now all the other actors
		int nbActors = zip.getInt();
		while (nbActors > 0) {
			Actor *actor = new Actor(0, 0, 0, NULL, TCODColor::white);
			actor->load(zip);
			actors.push(actor);
			nbActors--;
		}
		//finally the message log
		gui->load(zip);
		gameStatus = STARTUP;
	}
	else if (menuItem == Menu::GENERATE) {
#ifndef GENERATOR
#define GENERATOR
#endif
		stepThrough = true;
		engine.term();
		engine.init();
	}
}

void Engine::save()
{
	if (player->destructible->isDead()) {
		TCODSystem::deleteFile("game.sav");
	}
	else {
		TCODZip zip;
		//save the map
		zip.putInt(map->width);
		zip.putInt(map->height);
		map->save(zip);
		//save the player
		player->save(zip);
		//then the stairs
		stairs->save(zip);
		//then all other actors
		zip.putInt(actors.size() - 2);
		for (Actor **it = actors.begin(); it != actors.end(); it++) {
			if (*it != player && *it != stairs) {
				(*it)->save(zip);
			}
		}
		//now the message log
		gui->save(zip);
		zip.saveToFile("game.sav");
	}
}
