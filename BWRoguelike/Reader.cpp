//#include "main.hpp"
//#include "Reader.hpp"
#include "Reader.hpp"
#include <stdlib.h>
class Dungeonpak;

void Reader::ReadDungeons()
{
	//clear and then repopulate fileStr with contents of dungeons text file
	fileStr.clear();
	std::ifstream fileInput;
	fileInput.open("dungeons.txt");

	std::string token;
	while (std::getline(fileInput, token)) {
		std::size_t prev = 0, pos;
		while ((pos = token.find_first_of(":", prev)) != std::string::npos)
		{
			if (pos > prev)
				fileStr.push_back(token.substr(prev, pos - prev));
			prev = pos + 1;
		}
		if (prev < token.length())
			fileStr.push_back(token.substr(prev, std::string::npos));
	}
	printf("\nPrinting Dungeon.txt...\n");
	for (std::vector<std::string>::iterator it = fileStr.begin(); it != fileStr.end(); it++)
	{
		printf(it->c_str());
		printf("\n");
		if (it->c_str() == "N")
			packCount += 1;
	}	
	printf("Printing Complete. \n");
}

void Reader::PackDungeons(std::vector<Dungeonpak> &dpak)
{
	Dungeonpak temp;
	for (std::vector<std::string>::iterator it = fileStr.begin(); it != fileStr.end(); it++)
	{
		if (it->compare("N") == 0) {
			temp.name = (it + 1)->c_str();
			printf("New dungeonpak: ");
			printf(temp.name.c_str());
			printf("\n");
		}
		if (it->compare("S") == 0) {
			//pack the x and y sizes away
			temp.w = atoi((it + 1)->c_str());
			temp.h = atoi((it + 2)->c_str());
			printf("Width: %d, Height: %d", temp.w, temp.h);
			printf("\n");
		}
		if (it->compare("D") == 0) {
			temp.content.push_back((it + 1)->c_str());
			printf("DunCtnt: %s \n", temp.content.at(0).c_str());
		}

		printf("Iterator Index: %d \n", it - fileStr.begin());
		if (it->compare("-") == 0)
		{
		dpak.push_back(temp);
		}
	}
}

void Reader::init(std::vector<Dungeonpak> &dPaks)
{
	ReadDungeons();
	PackDungeons(dPaks);
	printf(dPaks.at(0).name.c_str());
}

/*std::vector<std::string>& Reader::split(const std::string & s, char delim, std::vector<std::string>& elems)
{
	// TODO: insert return statement here
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		if (item.length() > 0) {
			elems.push_back(item);
		}
	}
	return elems;
}

std::vector<std::string> Reader::split(const std::string & s, char delim)
{
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}*/


